FROM squidfunk/mkdocs-material as builder

COPY . /docs

RUN ["mkdocs", "build"]

FROM httpd

COPY --from=builder /docs/site /usr/local/apache2/htdocs/

EXPOSE 80